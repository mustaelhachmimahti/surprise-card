const button = document.getElementById('claim');
button.style.display = 'none';

async function cardData() {
  const randomCategories = Math.floor(Math.random() * (7 - 1)) + 1;
  switch (randomCategories) {
    case 1:
      var randomNum = Math.floor(Math.random() * (83 - 1)) + 1;
      var url = `https://swapi.dev/api/people/${randomNum}/`;
      break;
    case 2:
      var randomNum = Math.floor(Math.random() * (61 - 1)) + 1;
      var url = `https://swapi.dev/api/planets/${randomNum}/`;
      break;
    case 3:
      var randomNum = Math.floor(Math.random() * (7 - 1)) + 1;
      var url = `https://swapi.dev/api/films/${randomNum}/`;
      break;
    case 4:
      var randomNum = Math.floor(Math.random() * (38 - 1)) + 1;
      var url = `https://swapi.dev/api/species/${randomNum}/`;
      break;
    case 5:
      var randomNum = Math.floor(Math.random() * (40 - 1)) + 1;
      var url = `https://swapi.dev/api/vehicles/${randomNum}/`;
      break;
    case 6:
      var randomNum = Math.floor(Math.random() * (37 - 1)) + 1;
      var url = `https://swapi.dev/api/starships/${randomNum}/`;
      break;
  }

  try {
    const data = await fetch(url);
    const res = await data.json();
    const sizeRes = Object.keys(res).length;

    if (sizeRes !== 1) {
      const sentence = res.url;
      console.log(res);
      console.log(sentence);
      const card = document.getElementById('card');
      if (sentence.includes('starships')) {
        card.innerHTML = `  <div class="card-body">
       <h5 class="card-title">${res.name}</h5>
       <h6 class="card-subtitle mb-2 text-muted">Starships</h6>
       <p class="card-text">
         Passengers : ${res.passengers}
       </p>
       <p class="card-text">
       Model : ${res.model}
     </p>
       <p class="card-text">
       Max. atmospheric speed : ${res.max_atmospheric_speed}
       </p>
       <p class="card-text">
       Class : ${res.starship_class}
       </p>
       <a href="#" class="card-link">Card link</a>
       <a href="#" class="card-link">Another link</a>
     </div>`;
      }
      if (sentence.includes('people')) {
        if (res.species.length) {
          const specie = await fetch(res.species[0]);
          var nameSpecie = await specie.json();
        } else {
          var nameSpecie = {
            name: 'No specie found',
          };
        }
        card.innerHTML = `  <div class="card-body">
       <h5 class="card-title">${res.name}</h5>
       <h6 class="card-subtitle mb-2 text-muted">People</h6>
       <p class="card-text">
       Description: ${res.gender === 'female' ? 'Shi' : 'He'} is ${
          res.height
        } cm tall, with ${res.skin_color} skin and ${
          res.eye_color
        } eyes. About the hair color : ${res.hair_color}
       </p>
       <p class="card-text">
       Specie: ${nameSpecie.name}
       </p>
       <a href="#" class="card-link">Card link</a>
       <a href="#" class="card-link">Another link</a>
     </div>`;
      }
      if (sentence.includes('planets')) {
        card.innerHTML = `  <div class="card-body">
       <h5 class="card-title">${res.name}</h5>
       <h6 class="card-subtitle mb-2 text-muted">Planet</h6>
       <p class="card-text">
       Gravity : ${res.gravity}
       </p>
       <p class="card-text">
       Population : ${res.population}
       </p>
       <p class="card-text">
       Terrain: ${res.terrain}</p>
       <a href="#" class="card-link">Card link</a>
       <a href="#" class="card-link">Another link</a>
     </div>`;
      }
      if (sentence.includes('films')) {
        card.innerHTML = `  <div class="card-body">
       <h5 class="card-title">${res.title}</h5>
       <h6 class="card-subtitle mb-2 text-muted">Film</h6>
       <p class="card-text">
       Release date: ${res.release_date}
       </p>
       <a href="#" class="card-link">Card link</a>
       <a href="#" class="card-link">Another link</a>
     </div>`;
      }
      if (sentence.includes('species')) {
        card.innerHTML = `  <div class="card-body">
       <h5 class="card-title">${res.name}</h5>
       <h6 class="card-subtitle mb-2 text-muted">Species</h6>
       <p class="card-text">
       Description:It is ${res.height} cm tall, with ${res.skin_colors} skin and ${res.eye_colors} eyes. About the hair color : ${res.hair_colors}
       </p>
       <a href="#" class="card-link">Card link</a>
       <a href="#" class="card-link">Another link</a>
     </div>`;
      }
      if (sentence.includes('vehicles')) {
        card.innerHTML = `  <div class="card-body">
       <h5 class="card-title">${res.name}</h5>
       <h6 class="card-subtitle mb-2 text-muted">${res.model}</h6>
       <p class="card-text">
       Max_atmosphering_speed: ${res.max_atmosphering_speed}
       </p>
        <p class="card-text">
        passengers: ${res.passengers}
        </p>
        <p class="card-text">
        Cargo capacity: ${res.cargo_capacity}
        </p>
        <p class="card-text">
        Vehicle class: ${res.vehicle_class}</p>
       <a href="#" class="card-link">Card link</a>
       <a href="#" class="card-link">Another link</a>
     </div>`;
      }
    } else {
      card.innerHTML = `  <div class="card-body">
       <h5 class="card-title">No tienes suerte</h5>
       <h6 class="card-subtitle mb-2 text-muted">No tienes suerte</h6>
       <p class="card-text">
       No tienes suerte
       </p>
       <a href="#" class="card-link">Card link</a>
       <a href="#" class="card-link">Another link</a>
     </div>`;
    }
  } catch (e) {}
  countDownTime();
}
document.getElementById('claim').addEventListener('click', cardData);

// let countDownDate = new Date().getTime() + 02 * 60 * 60 * 1000;
let countDownDate = new Date().getTime() + 01 * 01 * 07 * 1000;
function countDownTime() {
  let x = setInterval(function () {
    let now = new Date().getTime();

    let timeLeft = countDownDate - now;

    const hours = Math.floor((timeLeft / (1000 * 60 * 60)) % 24);
    const minutes = Math.floor((timeLeft / 1000 / 60) % 60);
    const seconds = Math.floor((timeLeft / 1000) % 60);
    localStorage.setItem('hoursStore', hours);
    localStorage.setItem('minutesStore', minutes);
    localStorage.setItem('secondsStore', seconds);
    const hoursStore = localStorage.getItem('hoursStore');
    const minutesStore = localStorage.getItem('minutesStore');
    const secondsStore = localStorage.getItem('secondsStore');
    document.getElementById(
      'safeTimerDisplay'
    ).innerHTML = `${hoursStore}h ${minutesStore}m  ${secondsStore}s`;

    if (timeLeft < 0) {
      clearInterval(x);
      const button = document.getElementById('claim');
      button.style.display = 'block';
    }
    return timeLeft;
  }, 2000);
  return x;
}

countDownTime();
